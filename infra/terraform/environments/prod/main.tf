terraform {
  backend "s3" {
    bucket         = "suez-wts-terraform"
    key            = "environments/internal-prod/registrationserv/prod/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "suez-wts-terraform"
  }
}

provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::${var.aws_account_id}:role/automation"
  }

  region  = "${var.region}"
  version = "~> v1.18.0"
}

data "terraform_remote_state" "internal_prod_vpc" {
  backend = "s3"

  config {
    bucket = "suez-wts-terraform"
    key    = "environments/internal-preprod/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "internal_account" {
  backend = "s3"

  config {
    bucket = "suez-wts-terraform"
    key    = "accounts/swts-internal/terraform.tfstate"
    region = "us-east-1"
  }
}

module "registrationserv_prod" {
  source = "../../modules/registrationserv"

  # App Instance Inputs

  registrationserv_vpc_id             = "${data.terraform_remote_state.internal_prod_vpc.vpc_id}"
  registrationserv_public_subnets     = "${data.terraform_remote_state.internal_prod_vpc.vpc_public_subnets}"
  registrationserv_private_subnets    = "${data.terraform_remote_state.internal_prod_vpc.vpc_private_subnets}"
  registrationserv_data_subnets       = "${data.terraform_remote_state.internal_prod_vpc.vpc_data_subnets}"
  registrationserv_additional_tags    = "${var.global_tags}"
  registrationserv_environment        = "prod"
  registrationserv_ssl_certificate    = "arn:aws:acm:us-east-1:794033013208:certificate/748925ef-b2e2-4ce5-8373-ea5806cbb7ac"
  registrationserv_key_name           = "internal-prod-admin"
  registrationserv_admin_web_sg_id    = "${data.terraform_remote_state.internal_prod_vpc.admin_web_sg_id}"
  registrationserv_admin_linux_sg_id  = "${data.terraform_remote_state.internal_prod_vpc.admin_linux_sg_id}"
  registrationserv_web_instance_size  = "t2.medium"
  registrationserv_web_instance_count = 1
 # ec2_policy_for_ssm                  = "${data.terraform_remote_state.internal_account.ec2_policy_for_ssm}"
 # registrationserv_cr_sg_id           = "sg-0bf2d8622f945453d"
  ec2_policy_for_ssm                  = "${var.ec2_policy_for_ssm}"
  
}
output "instance_id" {
  value = "${module.registrationserv_prod.instance_id}"
}

output "target_group_id" {
  value = "${module.registrationserv_prod.target_group_id}"
}
