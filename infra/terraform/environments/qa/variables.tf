variable "region" {
  default = "us-east-1"
}

variable "aws_account_id" {
  default = "794033013208"
}

variable "environment" {
  default = "internal-preprod"
}

variable "global_tags" {
  type = "map"

  default = {
    "Customer"  = "suez-wts"
    "CreatedBy" = "terraform"
    "Environment" = "qa"
  }
}

variable "shared_keypair" {
  default = "shared_services_admin"
}

variable "ec2_policy_for_ssm" {
   default = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}