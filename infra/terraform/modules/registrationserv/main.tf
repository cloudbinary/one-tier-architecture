data "aws_vpc" "selected_vpc" {
  id = "${var.registrationserv_vpc_id}"
}

data "aws_subnet" "selected_public_subnets" {
  id    = "${element(var.registrationserv_public_subnets,count.index)}"
  count = 3
}

data "aws_subnet" "selected_private_subnets" {
  id    = "${element(var.registrationserv_private_subnets,count.index)}"
  count = 3
}

data "aws_subnet" "selected_data_subnets" {
  id    = "${element(var.registrationserv_data_subnets,count.index)}"
  count = 3
}

data "aws_ami" "selected_ami" {

  most_recent = true

  filter {
    name = "name"

    values = [
      "regservice-${var.regservice_version}",
    ]
  }

  filter {
    name   = "owner-id"
    values = ["${var.shared_services_ami_account}"]
  }
   filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


resource "aws_iam_instance_profile" "registrationserv_profile" {
  name = "${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}"
  role = "${aws_iam_role.registrationserv_role.name}"
}

resource "aws_iam_role" "registrationserv_role" {
  name = "${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
         "Service": ["ec2.amazonaws.com","ssm.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "registrationserv_ec2_ssm_policy_attachment" {
  role       = "${aws_iam_role.registrationserv_role.name}"
  policy_arn = "${var.ec2_policy_for_ssm}"
}

resource "aws_iam_role_policy_attachment" "codedeploy_ec2_policy_attachment" {
  role       = "${aws_iam_role.registrationserv_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}
resource "aws_iam_role_policy_attachment" "s3_ec2_policy_attachment" {
  role       = "${aws_iam_role.registrationserv_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// ALB

resource "aws_lb" "registrationserv_alb" {
  name            = "${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-alb"
  internal        = false
  subnets         = ["${data.aws_subnet.selected_public_subnets.*.id}"]
  security_groups = ["${aws_security_group.registrationserv_alb_sg.id}", "${var.registrationserv_admin_web_sg_id}", "${var.registrationserv_cr_sg_id}"]
  idle_timeout    = 60
  tags            = "${merge(var.registrationserv_additional_tags,var.registrationserv_module_tags,map("Name","${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}"))}"
}

resource "aws_lb_target_group" "registrationserv_alb_target_group" {
  name     = "${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-alb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${data.aws_vpc.selected_vpc.id}"

  health_check {
    path                = "/"
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 15
    matcher             = "200-499"
  }
}

resource "aws_lb_listener" "registrationserv_http_alb_listener" {
  load_balancer_arn = "${aws_lb.registrationserv_alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.registrationserv_alb_target_group.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "registrationserv_https_alb_listener" {
  load_balancer_arn = "${aws_lb.registrationserv_alb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = "${var.registrationserv_ssl_certificate}"

  //ssl_policy = ""

  default_action {
    target_group_arn = "${aws_lb_target_group.registrationserv_alb_target_group.arn}"
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "registrationserv_instance_alb" {
   target_group_arn = "${aws_lb_target_group.registrationserv_alb_target_group.arn}"
   target_id        = "${element(aws_instance.registrationserv_web.*.id,count.index)}"
   port             = 80
   count            = "${var.registrationserv_web_instance_count}"
 }

//INSTANCES

//Executing User Data based on environment

data "template_file" "registrationserv_user_data" {

  template = "${file("${path.module}/templates/web_userdata.tpl")}"

  vars {
    env          = "${var.registrationserv_environment}"
  }
}

//registrationserv WEB

resource "aws_instance" "registrationserv_web" {
  lifecycle {
    ignore_changes = ["ebs_block_device"]

  }

  ami           = "${data.aws_ami.selected_ami.id}"
  instance_type = "${var.registrationserv_web_instance_size}"
  key_name      = "${var.registrationserv_key_name}"
  subnet_id     = "${element(data.aws_subnet.selected_private_subnets.*.id,count.index)}"
  user_data     = "${data.template_file.registrationserv_user_data.rendered}"
  

  vpc_security_group_ids = [
    "${aws_security_group.registrationserv_web_sg.id}",
    "${var.registrationserv_admin_linux_sg_id}",
    "${var.registrationserv_cr_sg_id}",
    "sg-097a060902d79334b"
  ]

  iam_instance_profile        = "${aws_iam_instance_profile.registrationserv_profile.name}"
  associate_public_ip_address = false

  root_block_device {
    volume_size           = "${var.registrationserv_root_volume_size}"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags = "${merge(var.registrationserv_additional_tags,var.registrationserv_module_tags,
            map("Name","${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-web"),
            map("Backup","true"),
            map("CR_DiscoveredServices", "OS"),
            map("Environment", var.registrationserv_environment)

          )}"

  count = "${var.registrationserv_web_instance_count}"
}

//Security Groups

//ELB Security Group

resource "aws_security_group" "registrationserv_alb_sg" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix = "${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-elb"
  description = "Allow connections to elb"
  vpc_id      = "${data.aws_vpc.selected_vpc.id}"
  tags        = "${merge(var.registrationserv_additional_tags,var.registrationserv_module_tags,map("Name","${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-elb"))}"
}

resource "aws_security_group_rule" "registrationserv_http_alb_sg" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.registrationserv_alb_sg.id}"
}

resource "aws_security_group_rule" "registrationserv_https_alb_sg" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.registrationserv_alb_sg.id}"
}

resource "aws_security_group_rule" "registrationserv_alb_rule_outgoing" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.registrationserv_alb_sg.id}"

  cidr_blocks = [
    "0.0.0.0/0",
  ]
}

//Instance Security Group

//SG Instance WEB

resource "aws_security_group" "registrationserv_web_sg" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix = "${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-web-"
  description = "${var.registrationserv_resource_name_prepend} Security Group."
  vpc_id      = "${data.aws_vpc.selected_vpc.id}"

  tags = "${merge(var.registrationserv_additional_tags,var.registrationserv_module_tags,map("Name","${var.registrationserv_resource_name_prepend}-${var.registrationserv_environment}-web"))}"
}

resource "aws_security_group_rule" "registrationserv_sg_web_rule" {
  type      = "ingress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"

  security_group_id        = "${aws_security_group.registrationserv_web_sg.id}"
  source_security_group_id = "${aws_security_group.registrationserv_alb_sg.id}"
}

resource "aws_security_group_rule" "registrationserv_sg_rule_web_outgoing" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.registrationserv_web_sg.id}"

  cidr_blocks = [
    "0.0.0.0/0",
  ]
}

//Cloudwatch

resource "aws_cloudwatch_metric_alarm" "registrationserv_web_cloudwatch_recovery" {
  alarm_name                = "${var.registrationserv_environment}-registrationserv-web-${count.index}-status-check-failed"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "StatusCheckFailed_System"
  namespace                 = "AWS/EC2"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  alarm_description         = "This metric monitors ec2 status check"
  insufficient_data_actions = []

  alarm_actions = [
    "arn:aws:automate:us-east-1:ec2:recover",
  ]

  dimensions {
    InstanceId = "${element(aws_instance.registrationserv_web.*.id,count.index)}"
  }

  count = "${var.registrationserv_web_instance_count}"
}

output "instance_id" {
  value = "${aws_instance.registrationserv_web.*.id}"
}

output "web_sg_id" {
  value = "${aws_security_group.registrationserv_web_sg.id}"
}

output "target_group_id" {
  value = "${aws_lb_target_group.registrationserv_alb_target_group.arn}"
}

