variable "registrationserv_vpc_id" {}

variable "registrationserv_public_subnets" {
  type = "list"
}

variable "registrationserv_private_subnets" {
  type = "list"
}

variable "ec2_policy_for_ssm" {}

variable "registrationserv_data_subnets" {
  type = "list"
}

variable "regservice_version" {
  default = "3.0"
}

variable "registrationserv_root_volume_size" {
  default = 30
}

variable "registrationserv_web_instance_size" {
  default = "t2.medium"
}
variable "shared_services_ami_account" {
  default = "794033013208"
}

variable "registrationserv_web_instance_count" {
  default = 1
}

variable "registrationserv_key_name" {}

variable "registrationserv_environment" {}

variable "registrationserv_ssl_certificate" {}

variable "registrationserv_additional_tags" {
  type = "map"
}

variable "registrationserv_resource_name_prepend" {
  default = "registrationserv"
}

variable "registrationserv_module_tags" {
  type = "map"

  default = {
    "Application"  = "registrationserv"
    "ContactEmail" = "DL-WATS-AWSOps@suez.com"
  }
}

variable "registrationserv_assign_eip" {
  default = false
}

variable "registrationserv_admin_linux_sg_id" {}

variable "registrationserv_admin_web_sg_id" {}

variable "registrationserv_cr_sg_id" {
  default = "sg-0317c319433290ce1"

}

