#!/bin/bash

# Copying environment config files
cp -pvr /home/centos/registration/webconfig/${env}/oidc.conf /etc/httpd/conf.d/oidc.conf
cp -pvr /home/centos/registration/webconfig/${env}/vhost.conf /etc/httpd/conf.d/vhost.conf


sudo systemctl restart httpd.service

