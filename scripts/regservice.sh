#!/bin/bash
SERVICE="java"
if ! pgrep -x "$SERVICE" >/dev/null
then
        cd /home/centos/registration/target/
        nohup java -Dserver.port=8088 -Dspring.profiles.active=prod -jar gew-registration-service-0.0.1-SNAPSHOT.jar &
fi