STATUS="$(systemctl is-active httpd.service)"
if [ "$STATUS" == "unknown" ]; then
  echo "starting httpd service"
  systemctl enable httpd.service
  systemctl restart httpd.service
else
 echo "httpd is currently running"
fi
